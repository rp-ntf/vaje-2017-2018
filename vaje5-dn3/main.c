#include <stdio.h>

/* Sestavi program, ki prebere naravno število n
 * in izračuna vsoto njegovih števk,nato izračuna
 * vsoto števk dobljenega števila in tako naprej
 * dokler ne dobi enomestnega števila.
 * Dobljene vsote naj sproti izpisuje.
 * */

int main()
{
	
    int vsota = 0;
    int enice, n;
    
    printf ("Vpisi naravno stevilo: "); scanf("%d",&n);
    
   do {
        while(n > 0) {
            enice = n%10;
            vsota = vsota + enice;
            n = n/10;
        }    
        n = vsota;
        printf("%d\n",n);
        vsota = 0;
   } while (n > 9);
    

    
	return 0;
}
