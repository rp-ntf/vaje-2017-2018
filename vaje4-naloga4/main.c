#include <stdio.h>

/* Na zaslon izpiši zvezdice in sicer tako,
 * da je v prvi vrstici ena zvezdica, v
 * drugi dve, v tretji tri, itd.
 * Uporabnik vpiše maksimalno število 
 * zvezdic v vrstici.
 * */

int main()
{
    int i, j, n;
    
    printf("Vpisi stevilo n: "); scanf("%d",&n);
    
    for (i = 1; i <= n; i++) {
        for(j = 1; j <= i ; j++) printf("*");
        printf("\n");
    }

	return 0;
}
