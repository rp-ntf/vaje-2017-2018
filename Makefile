.PHONY: clean All

All:
	@echo "----------Building project:[ vaje11-naloga4 - Debug ]----------"
	@cd "vaje11-naloga4" && "$(MAKE)" -f  "vaje11-naloga4.mk"
clean:
	@echo "----------Cleaning project:[ vaje11-naloga4 - Debug ]----------"
	@cd "vaje11-naloga4" && "$(MAKE)" -f  "vaje11-naloga4.mk" clean
