#include<stdio.h>
/* Sestavi program, ki prebere naravno
 * število n (n<=99) in ga pretvori v rimsko številko.
 * (npr. 13 se pretvori v XIII, 14 v XIV, 19 v XIX
 * 21 v XXI, itd.) Program bere števila, dokler 
 * uporabnik ne vnese števila 0.
 * 
 * Namig : I=1, V=5, X=10, L=50
 * posebna pozornost : IV=4 IX=9 XL=40
 * */

int main(){
	
	int num,ostanek,stevec;

	printf("Vpisi stevilo: ");
	scanf("%d",&num);
	if (num <= 100) {
		for (stevec = 1; stevec <= num / 100; stevec++)
			printf("C");
		ostanek = num % 100;
		for (stevec = ostanek / 90; stevec == 1; stevec++)
			printf("XC");
		ostanek = ostanek % 90;
		for (stevec = ostanek / 50; stevec == 1; stevec++)
			printf("L");
		ostanek = ostanek % 50;
		for (stevec = ostanek / 40; stevec == 1; stevec++)
			printf("XL");
		ostanek = ostanek % 40;
		for (stevec = ostanek / 10; stevec >= 1; stevec--)
			printf("X");
		ostanek = ostanek % 10;
		for (stevec = ostanek / 9; stevec == 1; stevec++)
			printf("IX");
		ostanek = ostanek % 9;
		for (stevec = ostanek / 5; stevec == 1; stevec++)
			printf("V");
		ostanek = ostanek % 5;
		for (stevec = ostanek / 4; stevec == 1; stevec++)
			printf("IV");
		ostanek = ostanek % 4;
		for (stevec = ostanek; stevec >= 1; stevec--)
			printf("I");

		printf("\n");
	}
	else
		printf("Vneseno stevilo je preveliko");

	return 0;
}



	/*
	for(stevec=1;stevec<=num/1000;stevec++)
	printf("M");
	ostanek = num%1000;

	for(stevec=ostanek/900;stevec==1;stevec++)
	printf("CM");
	ostanek = ostanek%900;

	for(stevec=ostanek/500;stevec==1;stevec++)
	printf("D");
	ostanek = ostanek%500;

	for(stevec=ostanek/400;stevec==1;stevec++)
	printf("CD");
	ostanek = ostanek%400;
	*/