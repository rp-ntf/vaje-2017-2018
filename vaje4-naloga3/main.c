#include <stdio.h>

/* Izpiši tabelo produktov naravnih
 * števil do števila n
 * */

int main()
{
    int n;
    int i, j;
    
	printf("Vpisi stevilo n: "); scanf("%d",&n);
    
    for(i=1; i <= n; i++) {
        for(j=1;j<=n;j++) {
            printf("%d ",i*j);
        }
        printf("\n");
    }
    
	return 0;
}
