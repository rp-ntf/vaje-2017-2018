/*Sestavi funkcijo, ki prejme kot 
 * parameter velikost tabele, jo ustvari 
*  uporabo funkcije malloc ter vrne
* kazalec na to tabelo*/
/*Sestavi funkcijo, ki prejme kot parametre 
 * tabelo in njeno velikost ter ustvari dve novi 
 * tabeli in preko parametrov funkcije vrne njuni
 * velikosti in kazalca na njiju. V prvi naj bodo soda,
 * v drugi pa liha števila iz prvotne tabele. */
 
#include <stdio.h>
#include <stdlib.h>

int *UstvariTabeloInt(int n)
{
    int *tabela;
    tabela = (int *)malloc(n*sizeof(int));
    
    return tabela;
}

void SodaLiha(int **a, int **b, int *tab, int n, int *ns, int *nl)
{
    /* tab je dana tabela, *a je tabela sodih stevil in *b tabela
     * lihih stevil, n je stevilo elementov tabele tab, ns in nl pa 
     * stevilo elementov v drugih dveh tabelah */
     int st_lihih = 0;
     int j,k;
    
     for (int i = 0; i<n; i++) {
         if (tab[i]%2) st_lihih++;
     }
     
     *b = UstvariTabeloInt(st_lihih);  // tabela lihih stevil
     *a = UstvariTabeloInt(n - st_lihih);  // tabela sodih stevil
     *nl=st_lihih;
     *ns= n - st_lihih;
     j=0; k=0;
     for(int i=0;i<n; i++) 
     {
         if (tab[i]%2) {
             (*b)[j]=tab[i];
             j++;
         }
         else {
             (*a)[k]=tab[i];
             k++;
         }
     }
 }

int main()
{
	int a[]={12,3,55,4,2,8,88,99};
    int ns,nl;
    int *ks;
    int *kl;
    
     SodaLiha(&ks,&kl,a,8,&ns,&nl);
 
    
    printf("ns = %d nl = %d\n\n",ns,nl);
    for (int i=0; i<ns; i++) printf("s[%d]=%d\n",i,ks[i]);
    printf("\n");
    for (int i=0; i<nl; i++) printf("l[%d]=%d\n",i,kl[i]);
   
	return 0;
}
