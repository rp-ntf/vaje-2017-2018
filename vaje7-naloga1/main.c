#include <stdio.h>

int main()
{
    /* Naloga :
     * sestavite program, ki od uporabnika prebere celih 5 stevil.
     * Ta stevila naj shrani v tabelo.
     * Program naj naredi se dve tabeli, 
     * v tabelo 2. naj vstavi dvakratnike stevil v tabeli 1,
     * v tabelo 3. pa naj vstavi 3 kratnike stevil v tabeli 1.
     * Program naj na koncu izpise vse tri tabele,
     * z ustrezujocimi indeksi v isti vrstici.
     */
    
    int tabela1[5]; // tabela1[0] ... tabela1[4]
    int x;
    // <tip spremenljivke v tabeli> ime_spremenljivke[<velikost>];
    // tabela1[3]; // se obnasa kot spremenljivka tipa int
    printf("Vnesite 5 celih stevil v tabelo : \n");
    for( int i=0; i<5; ++i )
    {
        printf("Vnesite stevilo %d : ", i );
        // scanf("%d", &x ); // preberemo vnos v spremenljivko x
        scanf("%d", &tabela1[i] ); // preberemo vnos v spremenljivko x
    }
    int tabela2[5];
    int tabela3[5];
    for( int i=0; i<5; ++i )
    {
        tabela2[i] = 2*tabela1[i];
        tabela3[i] = 3*tabela1[i];
    }
    for( int i=0; i<5; ++i )
    {
        // printf("tabela1[%d] = %d\n", i, tabela1[i] );
        printf("%d : %d | %d | %d \n", i, 
            tabela1[i], tabela2[i], tabela3[i] );
    }
	return 0;
}


