#include <stdio.h>

/*
 * Sestavite program, ki od uporabnika prebere število n,
 * in izpiše vsoto sodih kvadratov do števila n manj 
 * vsoto lihih kvadratov, do števila n.
 * Npr : 
 * n = 10 
 * program izpiše število -6
 * prištejem sode kvadrate : +0+4
 * odštejem lihe kvadrate : +1+9
 * rezultat = -6
 * 
 */

int main()
{
    printf("Vnesite stevilo n : ");
    int n;
    scanf("%d", &n);
    
    int sodi = 0, lihi = 0;
    for( int i=0; i*i<n; i = i+2 )
    {
        sodi += i*i;
    }
    for( int i=1; i*i<n; i = i+2 )
    {
        lihi += i*i;
    }
    printf("n = %d, vsota sodih kvadratov = %d, vsota lihih kvadratov = %d\n",
        n, sodi, lihi );
    printf("%d\n", sodi - lihi ); // koncen izpis
	return 0;
}
