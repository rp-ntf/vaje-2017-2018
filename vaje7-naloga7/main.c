#include <stdio.h>
#include <string.h>
int main()
{
	/* Naloga : 
	 * Naredite program, ki od uporabnika v zanki 
	 * prebira niz, in izpise ta niz skupaj z njegovo dolzino. 
	 * Program se konca, ko uporabnik vpise niz "konec".
	 */
    char niz[256];
    char konec_niz[] = "konec";
    while( strcmp( niz, konec_niz ) != 0 )
    {
        printf("Vnesite niz znakov : ");
        scanf("%s", niz );
        printf("Vpisali ste niz #%s#, dolzine %d.\n",
            niz, strlen(niz) );
    }
    return 0;
	/* Domaca naloga : 
	 * Naredite program, 
	 * ki od uporabnika prebere niz znakov, 
	 * a) in izpise, kolikokrat se v njem pojavi crka 'a'.
     * for( int i=0; i<strlen(niz); ++i )
     * { if( niz[i] == 'a' ) stevec++ }
	 * b) in izpise, kolikokrat se v njem pojavi samoglasnik. 
	 * Primer : 
	 * To je testen stavek 
	 * stevilo samoglasnikov : 6
	 */
	 
	/* Domaca naloga : 
	 * Naredite program, ki od uporabnika prebere niz znakov (beseda).
	 * Program potem izpise niz znakov v obratnem vrstenm redu.
	 * Primer : abcdefg -> gfedcba
	 */
}
