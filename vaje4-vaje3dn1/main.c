#include <stdio.h>

/* Sestavi program, ki prebere naravno število
 * in izračuna produkt njegovih neničelnih števk
 */

int main()
{
    int n, preostanek, stevka, produkt;
    printf("Vnesite stevilo : \n");
    scanf("%d",&n);
    produkt = 1;
    preostanek = n;
    while( preostanek > 0 )
    {
        stevka = preostanek % 10;
        preostanek = preostanek / 10;
        if( stevka != 0 )
            produkt *= stevka;
    }
	printf("Produkt nenicelnih stevk stevila %d je %d.\n", n, produkt);
	return 0;
}
