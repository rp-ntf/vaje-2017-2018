#include <stdio.h>

int main()
{
	/* Sestavite program, 
	 * ki prebere dve stevili, 
	 * in vrne absolutno vrednost njune razlike
	 */
    
	float x, y, razlika;
    printf("Vnesite prvo stevilo : ");
    scanf("%f", &x);
    printf("Vnesite drugo stevilo : ");
    scanf("%f", &y );
    
	// printf("Vnesite dve stevili : \n");
	// scanf("%f %f", &x, &y);
    if ( x > y )
    {
        razlika = x - y;
        printf("Blok stavek A\n");
        printf("Blok stavek B\n");
        if( x > 10 )
        {
            printf(" x > 10 \n");
            if( x > 20 )
            {
                printf("x > 20\n");
            }
        }
    }
    else 
        razlika = y - x;
    printf("|x-y| = %f\n", razlika );
    
    // blok stavki
    // crke v nizu
    
    return 0;
}