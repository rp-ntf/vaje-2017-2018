#include <stdio.h>

/* Sestavi program, ki prebere naravno število N 
 * in prešteje ter izpiše vse njegove delitelje 
 */
 
int main()
{
    int n;
    int i = 0;
    int delitelj = 1;
    
	printf("Vpisi naravno stevilo: \n");
    scanf("%d",&n);
    
    while (delitelj <= n) {
        if (n%delitelj == 0) {
            printf("%d\n",delitelj);
            i = i + 1;
        }
        delitelj = delitelj + 1;
    }
    
    printf("Stevilo %d ima %d deliteljev.\n", n,i);
    
	return 0;
}
