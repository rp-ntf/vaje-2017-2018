#include <stdio.h>

int main()
{
	/* Domaca naloga :
	 *
	* Uporabnika vprasajte za znesek (naravno stevilo).
	* Program naj izpise, kako lahko ta znesek
	* izplacate z najmanjsim skupnim stevilom bankovcev,
	* kjer lahko uporabimo bankovce in kovance
	* 200, 100, 50, 20, 10, 5, 2, 1.
	*/
	printf("Vnesite znesek : ");
	int znesek, i;
	scanf("%d", &znesek);
	int tabela[9] = { 200, 100, 50, 20, 10, 5, 2, 1 };
	printf("Izplacujemo : %d\n", znesek);
	while (znesek > 0)
	{
		for (i = 0; i<9; ++i)
		{
			if (znesek >= tabela[i])
				break;
		}
		znesek = znesek - tabela[i];
		printf("%d, ostane se %d\n", tabela[i], znesek);
	}
	printf("\n");
	return 0;
}

