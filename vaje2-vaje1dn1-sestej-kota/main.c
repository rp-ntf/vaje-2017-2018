#include <stdio.h>

/*
 * Sestavite program, 
 * ki od uproabnika prebere dva kota v radianih. 
 * Kota seštejte in izpišite rezultat v stopinjah, minutah in sekundah.

Namig : lahko ponovno uporabite kodo za pretvorbo med jardi in metri.
 */

int main()
{
	float kot1, kot2;
	printf("Vnesite prvi kot v radianih : ");
	scanf("%f", &kot1);
	printf("Vnesite drugi kot v radianih : ");
	scanf("%f", &kot2);
	printf("Izracunali bomo vsoto kotov %f + %f v stopinjah\n", kot1, kot2 );
	
	float vsota = (kot1 + kot2)*180/3.141592653589793; // vsota v stopinjah z decimalkami
	int stopinje, minute, sekunde;
	stopinje = (int)vsota;
	vsota = 60*(vsota-stopinje);
	minute = (int)vsota;
	vsota = 60*(vsota-minute);
	sekunde = (int)vsota;
	
	printf("Vsota kotov je %d stopinj %d minut %d kotnih sekund\n", 
		stopinje, minute, sekunde );
	
	return 0;
}
