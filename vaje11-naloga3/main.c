#include <stdio.h>
/*
 * Naloga : 
 * V funkciji preberite datoteko, 
 * ki smo jo ustvarili v prejsni 
 * nalogi. 
 * Iz stolpca vrednosti y naj ta funkcija 
 * izracuna povprecno vrednost in jo vrne.
 * Funkcija naj izpise stevilo prebranih vrstic.
		fprintf( fOut, "%f; %f\n", x, sin(x) / ( x*x + 1 ) );
		fscanf( fIn, "%f; %f", &x, &y ) == EOF;
*/
/* Naloga : 
 * Privzamemo, da je datoteka dolga najvec 1000 vrstic. 
 * Funkcijo spremenite tako, da po branju datoteke
 * izpise vrednosti x in y v novo datoteko. 
 * Vrednosti x naj bodo zapisane v enakem vrstnem redu v 1. stolpcu, 
 * vrednosti y naj bodo zapisane v enakem vrstnem redu v 2. stolpcu, 
 * vrednosti y naj bodo zapisane v obratnem vrstnem redu v 3. stolpcu. 
 * 
 * Domača naloga : 
 * iz vhodne datoteke, kot smo jo ustvarili v prejsnem programu, 
 * naredite datoteko, ki le zamenja stolpca x in y
 *
 */

void povprecjeStolpcaY( 
    char * imeDatoteke 
)
{
    FILE * f = fopen(imeDatoteke, "rt" );
    if( f == NULL )
    {
        printf("NAPAKA pri odpiranju datoteke!\n");
        return;
    }
    float x,y;
    float vsota = 0;
    int n=0;
    while( fscanf( f, "%f %f", &x, &y ) != EOF )
    {
        vsota += y;
        n++;
    }
    printf("Prebrali smo %d vrstic.\n", n );
    printf("Povprecje je %f.\n", vsota / n );
}

void preberiInObrniY(
char * imeDatotekeIn,
char * imeDatotekeOut
)
{
    FILE * f = fopen(imeDatotekeIn, "rt" );
    float tabX[1000], tabY[1000];
    if( f == NULL )
    {
        printf("NAPAKA pri odpiranju datoteke!\n");
        return;
    }
    float x,y;
    int n=0;
    while( fscanf( f, "%f %f", &x, &y ) != EOF )
    {
        tabX[n] = x;
        tabY[n] = y;
        n++;
    }
    printf("Prebrali smo %d vrstic.\n", n );
    
    FILE * fOut = fopen(imeDatotekeOut, "wt" );
    if (fOut == NULL )
    {
        printf("NAPAKA pri odpiranju datoteke za pisanje!\n");
        return;
    }
    for( int i=0; i<n; ++i )
        fprintf(fOut, "%f %f %f\n", tabX[i], tabY[i], tabY[n-i-1] );
    fclose(fOut);    
}

int main()
{
    povprecjeStolpcaY("../vaje11-naloga2-ponavljanje/vrednosti2.csv");
    
    preberiInObrniY("../vaje11-naloga2-ponavljanje/vrednosti2.csv",
        "vrednosti3.csv");
    return 0;
}








/*
float povprecjeSeznama( 
	char * imeDatoteke )
{
	FILE * fIn = fopen( imeDatoteke, "rt" );
	if( fIn == NULL )
	{
		printf("Napaka pri odpiranju datoteke !\n");
		return 0.0;
	}
	float x, y;
	int n = 0;
	float vsota = 0;
	while( fscanf( fIn, "%f; %f", &x, &y ) != EOF )
	{
		n++;
		vsota += y;		
	}
	printf("Prebrali smo %d vrstic.\n", n );
	return vsota / n;
}

float dodajStolpec( 
	char * imeDatoteke,
	char * imeNoveDatoteke )
{
	float tabelaX[501], tabelaY[501];
	FILE * fIn = fopen( imeDatoteke, "rt" );
	if( fIn == NULL )
	{
		printf("Napaka pri odpiranju datoteke !\n");
		return 0.0;
	}
	float x, y;
	int n = 0;
	float vsota = 0;
	while( fscanf( fIn, "%f; %f", &x, &y ) != EOF )
	{
		tabelaX[n] = x;
		tabelaY[n] = y;
		n++;
		vsota += y;		
	}
	printf("Prebrali smo %d vrstic.\n", n );
	FILE * fOut = fopen( imeNoveDatoteke, "wt" );
	if ( fOut == NULL )
	{
		printf("Napaka pri odpiranju datoteke za pisanje ");
		return 0.0;
	}
	int i;
	for( i=0; i<n; ++i )
	{
		fprintf( fOut, "%f; %f; %f\n", tabelaX[i], tabelaY[i], tabelaY[n-i-1] );
	}
	fclose(fOut);
	return vsota / n;
}

int main(int argc, char **argv)
{
	float povprecje = povprecjeSeznama( "funkcija2.csv" );
	printf("Povprecje je %f.\n", povprecje );
	dodajStolpec( "funkcija2.csv", "funkcija3.csv" );
	return 0;
}
*/