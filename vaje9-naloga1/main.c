#include <stdio.h>

void IzpisTabeleObratno(int vt, int tabela[]);
void IzpisLihihIndeksovTabele(int vt, int tabela[])
{
    for(int i = 0; i < vt; i++) {
        if (i%2) printf("a[%d] = %d\n", i, tabela[i]);
    }
}

int main()
{
	int a[] = {2,4,56,27,-1,-25};
    
    IzpisTabeleObratno(6,a);
    printf("\n");
    IzpisLihihIndeksovTabele(6,a);
	return 0;
}

void IzpisTabeleObratno(int vt, int tabela[])
{
    for (int i = vt-1; i >=0; i--)
      printf("a[%d] = %d\n", i, tabela[i]);
}