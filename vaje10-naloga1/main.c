/* Sestavi funkcijo, ki ciklično zamenja 
 * tri cela števila a, b, c in sicer: a->b, b->c, c->a */
 
#include <stdio.h>

void CiklicnoZamenjaj(int *a, int *b, int *c)
{
    int tmp;
    tmp = *b;
    *b = *a; 
    *a = *c;
    *c = tmp;
}
int main()
{
	int x=1, y=2, z=3;
    CiklicnoZamenjaj(&x,&y,&z);
    printf("x=%d y=%d z=%d\n",x,y,z);
    
	return 0;
}
