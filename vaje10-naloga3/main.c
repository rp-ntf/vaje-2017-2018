/* V datoteko zapisi prvih deset sodih stevil */

#include <stdio.h>
void ZapisSoda(char *imedat)
{
 	int i;
    FILE *out = fopen(imedat, "w");
    
    for (i=0; i<10;i++) fprintf(out,"%d\n",2*(i+1));

    fclose(out);   
}

void BranjeSoda(char *imedat)
{
 	int i,x;
    FILE *out = fopen(imedat, "r");
    
    for (i=0; i<10;i++) {
        fscanf(out,"%d",&x);
        printf("%d\n",x);
    }

    fclose(out);   
}
int main()
{
    ZapisSoda("../soda.txt");
    BranjeSoda("../soda.txt");
	return 0;
}
