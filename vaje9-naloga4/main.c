/* Z uporabo funkcije malloc dinamično deklariraj 
 * void *malloc(<število bajtov>)
 * tabelo realnih števil z petimi elementi */

#include <stdio.h>

int main()
{
	float *tabela;
    
    tabela = (float *)malloc(5*sizeof(float));
    
    tabela[0]=1.2;
    *(tabela+1)=3.2;
    for(int i=2; i<5; i++) tabela[i]=tabela[0]*2.0;
    
    for(int j = 0; j<5;j++) printf("tabela[%d] = %g\n",j,tabela[j]);
    
    free(tabela);
    
	return 0;
}
