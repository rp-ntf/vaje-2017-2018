#include <stdio.h>

/*
 * Naloga:
 * Napišite funkcijo, ki zapiše tabelo celih števil na disk
 * v tekstovno datoteko, po eno število v vrstico.
 * 
 * Naloga:
 * Napišite funkcijo, ki iz diska prebere tabelo celih števil,
 * zapisano v prvi funkciji.
 * 
 * Naloga:
 * Npaišite funkcijo, ki na disk zapiše dve tabeli enake dolžine,
 * v isto datoteko po stolpcih.
 * 
 * Naloga:
 * Napišite funkcijo, ki iz diska prebere dve tabeli enake dolžine, 
 * zapisani v prejšni funkciji.
 */

void zapisiTabeloVDatoteko(
    char * imeDatoteke, 
    int tabela[],
    int n )
{
    FILE * f = fopen(imeDatoteke, "wt" );
    if( f == NULL )
    {
        printf("Napaka pri odpiranju datoteke\n");
        return;
    }
    for( int i=0; i<n; ++i )
        fprintf(f, "%d\n", tabela[i]);
    fclose(f);
}

int preberiTabelo(
    char * imeDatoteke, 
    int tabela[] )
{
    FILE * f = fopen(imeDatoteke, "rt" );
    if( f == NULL )
    {
        printf("Napaka pri odpiranju datoteke\n");
        return -1;
    }
    int n=0;
    for( n=0; 1; ++n )
    {
        if( fscanf(f, "%d", &tabela[n]) == EOF )
            break;
    }
    fclose(f);
    return n;
}


int main()
{
    int tabela[10] = { 1, 2, 3, 4, 5, 16, 17, 18, 19, 20 };
    zapisiTabeloVDatoteko( "tabela.txt", tabela, 10 );
    
    int prebranaTabela[1000];
    int n;
    n = preberiTabelo( "tabela.txt", prebranaTabela );
    for( int i=0; i<n; ++i )
        printf("preberiTabelo[%d] = %d\n", i, prebranaTabela[i] );
	return 0;
}
