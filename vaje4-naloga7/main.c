#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* Sestavi program, ki naključno izbere število
 * med 1 in 100 ter zahteva od uporabnika, 
 * da to število ugane.
 * */

int main()
{
    float stevilo;
    int racunalnik;
    int uporabnik;
    int i=0;
    srand((unsigned)time(NULL));
    
    stevilo = (float)rand()/RAND_MAX;
    racunalnik = 1 + (int)(100*stevilo);
    
    printf("Izmislil sem si stevilo med 1 in 100. Ugani ga!\n");
    do {
        i++;
        printf("%d. poskus: ",i); scanf("%d",&uporabnik);
        if (racunalnik > uporabnik)
            printf("Narobe! Moje stevilo je vecje. \n");
        if (racunalnik < uporabnik)
            printf("Narobe! Moje stevilo je manjse. \n");
    } while (racunalnik != uporabnik);
    printf("Bravo! Uganil si v %d poskusih.\n",i);
	
	return 0;
}
