/* Sestavi funkcijo, ki v programu zamenja
 * vrednost dveh spremenljivk */

#include <stdio.h>

void Zamenjaj(int x, int y)
{
    int tmp = x;
    x = y;
    y = tmp;
    
    printf("znotraj f: a = %d, b = %d\n",x,y);
}

void ZamenjajK(int *x, int *y) 
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}
int main()
{
	int a = 5;
    int b = 10;
    printf("a = %d,  b = %d\n",a,b);
    //Zamenjaj(a,b);
    ZamenjajK(&a,&b);
    printf("a = %d,  b = %d\n",a,b);
	return 0;
}
