#include <stdio.h>

void Zapisi(char * imedat)
{
	int x = 10;
    FILE *idat;
    
    idat = fopen(imedat,"w");
    if (idat==NULL) printf("Napaka pri odpiranju datoteke!\n");
    else {
        fprintf(idat,"%d",x);
        printf("Uspesno zapisano.\n");
    }
    fclose(idat);
}

void Beri(char *imedat)
{
    int x;
    FILE *vdat = fopen(imedat, "r");
    if (vdat == NULL) printf("Napaka pri odpiranju datoteke!\n");
    else {
        fscanf(vdat,"%d",&x);
        fclose(vdat);
        printf("Prebrali smo stevilo %d\n",x);
    }
}

int main()
{
  //  Zapisi("noviizhod.txt");
  Beri("noviizhod.txt");

	return 0;
}
