#include <stdio.h>

struct kompleksnoINT {
    int Re;
    int Im; 
};

int main()
{
	struct kompleksnoINT a,b,c;
    
    a.Re = -1;
    a.Im = 3;
    b.Re = 2;
    b.Im = 1;
    c.Re = a.Re + b.Re;
    c.Im = a.Im + b.Im;
    
    printf("a+b = %d + i*%d\n",c.Re,c.Im);
	return 0;
}
