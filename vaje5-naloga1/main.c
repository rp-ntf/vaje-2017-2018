#include <stdio.h>

/* sestavi program v katerem bo uporabnik vnašal
 * cela števila, dokler ne vnese števila 0. Na koncu naj 
 * program izpiše povprečje pozitivnih vnešenih 
 * števil
 * */

int main()
{
	int n, vsota = 0, stevec = 0;
    float povprecje;
    
    while (1) {
        printf("Vpisi stevilo: "); scanf("%d",&n);
        if (n < 0) continue;
        vsota = vsota + n;
        if ( n == 0) break;
        stevec++;
    }
    
    if (stevec == 0) stevec = 1;
    povprecje = (float)vsota/stevec;
    
    printf("Povprecna vrednost je %g\n",povprecje);
    
	return 0;
}
