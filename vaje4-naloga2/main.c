#include <stdio.h>

/* Izpiši tabelo kvadratov celih števil do n */

int main()
{
	int n;
    
    printf("Vpisi stevilo n: "); scanf("%d",&n);
    
    for (int i=1; i <= n; i++) {
        printf("%d x %d = %d\n",i,i,i*i);
    }
	return 0;
}
