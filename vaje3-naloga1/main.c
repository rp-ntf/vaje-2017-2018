#include <stdio.h>

// vaja3 zanka while
// Napiši program, ki izpiše cela števila med 0 in 9 na zaslon


int main()
{
	int i = 0;
    
    while (i <10) {
        if (i%2 != 0)
            printf("%d\n",i);
        i = i + 1;
    }
    
	return 0;
}
