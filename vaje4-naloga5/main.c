#include <stdio.h>
/* Na zaslon izpiši zvezdice in sicer tako,
 * da bodo tvorile pravokotnik velikosti 
 * m x n zvezdic. Parametra m in n poda 
 * uporabnik: m = dolžina, n = višina.
 * */

int main()
{
    int m, n;
    int i, j;
    
    printf("Vpsi dolzino m = "); scanf("%d",&m);
    printf("Vpsi visino n = "); scanf("%d",&n);
    
    for (i = 0; i < n; i++) {
        for(j = 0; j < m; j++) printf("*");
        printf("\n");
    }
	
	return 0;
}
