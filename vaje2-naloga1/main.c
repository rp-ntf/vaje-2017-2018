// Ponavljanje

// Sturktura programa
// 1. vkljucimo vse knjiznice, ki jih potrebujemo (printf/scanf je v stdio.h)
#include <stdio.h>
/*
 * int main() // Glavna funkcija programa
 * {
 * ... 
 * <koda>
 * ...
 * return 0;
 * }
 */
int main()
{
	// Izpisovanje na zaslon :
	// printf( "niz znakov s formatnimi dolocili", x, y, z, ...<spremenljivke> )
	printf("Pozdravljen spet (ze spet)\n"); // Vsak stavek se zakljuci s ";" 
	/*
	 * Posebni znaki v printf : 
	 * \n : nova vrstica
	 * \r : vrnitev na začetek vrstice
	 * \b : (bell) zvonec
	 * \" : narekovaj "
	 * \\ : zanek "\"
	 * \' : narekovaj '
	 * %% : izpiše znak '%'
	 */
	printf("Pozdravljen\nv\nnekaj\nvrsticah!\n");
	printf("Z narekovaji \" in \'\n");
	 
	// spremenljivke :
	int m; // deklaracija
	int n = 10; // initializacija z deklaracijo
	printf("m = %d, n = %d\n", m, n ); // izpis spremenljivk
	m = 10*n; // množenje
	printf("m = %d\n",m);
	m = 17 % 5; // ostanek pri deljenju (operacija modulo)
	printf("m = %d\n",m);
	
	// Branje podatkov od uporabnika
	printf("Vnesite prvo celo stevilo m : ");
	scanf("%d", &m); // Pozor ! "&m", %d - cela stevila, %f - realna stevila
	printf("Vnesite drugo celo stevilo : ");
	scanf("%d", &n );
	printf("Produkt je %d\n", m*n);
	printf("Vsota je %d\n", m+n);
	printf("Ostanek pri deljenju m%%n = %d\n", m%n);
		
	return 0;
}
