#include <stdio.h>

int main()
{
	int x = 10;
    int *k = &x;
    
    printf("x = %d, x(k) = %d, nalov od x = %p\n",x, *k, k);
	return 0;
}
