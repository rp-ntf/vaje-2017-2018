/* Domaca naloga : 
 * Naredite program, 
 * ki od uporabnika prebere niz znakov, 
 * a) in izpise, kolikokrat se v njem pojavi crka 'a'.
 * for( int i=0; i<strlen(niz); ++i )
 * { if( niz[i] == 'a' ) stevec++ }
 * b) in izpise, kolikokrat se v njem pojavi samoglasnik. 
 * Primer : 
 * To je testen stavek 
 * stevilo samoglasnikov : 6
 */
#include <stdio.h>
#include <string.h>
int main(int argc, char **argv)
{
    char vrstica[256];
    // Bonus : 
    // char vrstice[5][256];
    // scanf( "%s", vrstice[i] );
    printf("Vnesite besedo : \n");
    scanf("%s", vrstica ); // !! Ni &vrstica kot pri "int"
    printf("Vnesli ste besedo #%s#\n", vrstica );
    int stevec = 0;
    for( int i=0; i<strlen(vrstica); ++i )
    { 
        if( vrstica[i] == 'a' ) 
            stevec++;
    }
    printf("Beseda \"%s\" ima %d \"a\"-jev.\n",
        vrstica, stevec );
    // char samoglasniki[] = { 'a', 'e', 'i', 'o', 'u' };
    char samoglasniki[] = "aeiou";
    stevec = 0;
    for( int i=0; i<strlen(vrstica); ++i )
    {
        for( int j=0; j<strlen(samoglasniki); ++j )
            if( vrstica[i] == samoglasniki[j] )
                stevec++;
    }
    printf("Beseda \"%s\" ima %d samoglasnikov.\n",
        vrstica, stevec );
    // Naloga : spremeni program, da izpise
    // kolikokrat se pojavi vsak samoglasnik.
    int stevci[] = { 0, 0, 0, 0, 0 };
    for( int i=0; i<strlen(vrstica); ++i )
    {
        for( int j=0; j<strlen(samoglasniki); ++j )
            if( vrstica[i] == samoglasniki[j] )
                stevci[j]++;
    }
    printf("Beseda \"%s\" ima :\n", vrstica );
    for( int j=0; j<strlen(samoglasniki); ++j )
    {
        printf("%d %c-jev\n", 
            stevci[j], samoglasniki[j] );
    }
    // Bonus naloga : od uproabnika najprej preberite 5 besed
    // in za vsako posebaj izpisite, koliko je v njej
    // katerih samoglasnikov
	return 0;
}
