#include <stdio.h>
#include <math.h>
/* Naloga : 
 * V datoteko zapisite vrednosti funkcije 
 * f(x) = sin(x) / (x^2 + 1 )
 * na intervalu [0,5]. V 2 stolpca izpisite 
 * x f(x)
 * Vrednosti izpisujte v korakih po 0.1
 * 
 * Nadaljevanje : 
 * Zapis v datoteko premaknite v funkcijo. 
 * Parametri funkcije naj bodo : 
 * a, b in dx ( interval izpisa in korak izpisa )
 * in ime datoteke za zapis funkcije
 */

void zapisiVdatoteko( 
    float a, float b, float dx,
    char * imeDatoteke )
{
    FILE * f = fopen( imeDatoteke, "wt" );
    float x;
    if( f == NULL )
    {
        printf("NAPAKA pri odpiranju datoteke!\n");
        return;
    }
    for( x = a; x < b+dx/2.0; x += dx )
        fprintf(f,"%f %f\n", x, sin(x)/(x*x+1) );
    fclose(f);
    return;
}

int main()
{
    zapisiVdatoteko( 0, 5, 0.1, "vrednosti2.csv" );
    return 0;
    
    FILE * f = fopen( "vrednosti1.csv", "wt" );
    float x;
    if( f == NULL )
    {
        printf("NAPAKA pri odpiranju datoteke!\n");
        return 1;
    }
    for( x = 0; x < 5.01; x += 0.1 )
        fprintf(f,"%f %f\n", x, sin(x)/(x*x+1) );
    fclose(f);
    return 0;
}
/*

void zapisiSeznam( 
	float a, float b, float dx, 
	char * imeDatoteke )
{
	FILE * fOut = fopen( imeDatoteke, "wt" );
	if( fOut == NULL )
	{
		printf("Napaka pri odpiranju datoteke !\n");
		return;
	}
	float x = 0;
	for( x = a; x < b + dx/2; x += dx )
	{
		fprintf( fOut, "%f; %f\n", x, sin(x) / ( x*x + 1 ) );
	}
}

int main(int argc, char **argv)
{
	zapisiSeznam( 0, 5, 0.1, "funkcija2.csv" );
	return 0;
	
	FILE * fOut = fopen("funkcija.csv", "wt" );
	if( fOut == NULL )
	{
		printf("Napaka pri odpiranju datoteke !\n");
		return 0;
	}
	float x = 0;
	float dx = 0.1;
	for( x = 0; x < 5.0 + dx/2; x += dx )
	{
		fprintf( fOut, "%f; %f\n", x, sin(x) / ( x*x + 1 ) );
	}

	return 0;
}
*/