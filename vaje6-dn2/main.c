#include <stdio.h>
/*
 * Sestavite program, ki od uporabnika prebere število n,
 * in na zaslon izpiše takšno piramido : 
 *       1
 *     232
 *   34543
 * 4567654
 * 
 * za n=4
 */

int main()
{
	int i, space, rows, k = 0, count = 0, count1 = 0;

	printf("Vpisi stevilo vrstic: ");
	scanf("%d", &rows);

	for (i = 1; i <= rows; ++i)
	{
		for (space = 1; space <= rows - i; ++space)
		{
			printf("  ");
			++count;
		}

		while (k != 2 * i - 1)
		{
			if (count <= rows - 1)
			{
				printf("%d ", (i + k)%10);
				++count;
			}
			else
			{
				++count1;
				printf("%d ", (i + k - 2 * count1)%10);
			}
			++k;
		}
		count1 = count = k = 0;

		printf("\n");
	}
	return 0;
}