#include <stdio.h>

/* Program prebere naravno število N in izpiše
 * kvadrate vseh sodih števil med 1 in N.
 */
 
 
int main()
{
    int n, i=1;
    
	printf("Vpisi naravno stevilo: "); scanf("%d",&n);
    
    while (i < n) {
        if (i%2 == 0)  printf("%d\n",i*i);
        i++;
    }
    
	return 0;
}
