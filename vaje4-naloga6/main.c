#include <stdio.h>

/* Sestavi program, ki petvarja milje 
 * v kilometre in vsakič vpraša,
 * ali naj nadaljujem, ali ne.
 * */

int main()
{
	int pogoj;
    float vrednost;
    
    do {
        printf("Vpisi milje: "); scanf("%f", &vrednost);
        printf("%g milj je %g km\n",vrednost,
            vrednost*1.609344);
        printf("Nadaljujem (1-da, 0-ne)?\n");
        scanf("%d",&pogoj);
    } while(pogoj);
    
	return 0;
}
