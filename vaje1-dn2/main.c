#include <stdio.h>

int main(int argc, char **argv)
{
	/* Navodilo : 
	 * Sestavi program, ki od uporabnika prebere 2 kota v stopinje, minutah in sekundah, 
	 * ju sešteje in izpiše rezultat 
	 */
	int stopinja1, stopinja2, minuta1, minuta2, sekunda1, sekunda2;
	printf("Vpisite prvi kot [ stopinja minuta sekunda ] :\n");
	scanf( "%d %d %d", &stopinja1, &minuta1, &sekunda1 );
	printf("Vpisite drugi kot [ stopinja minuta sekunda ] :\n");
	scanf( "%d %d %d", &stopinja2, &minuta2, &sekunda2 );
	
	int stopinja, minuta, sekunda;
	sekunda = sekunda1 + sekunda2;
	minuta = minuta1 + minuta2;
	stopinja = stopinja1 + stopinja2;
	
	minuta = minuta + sekunda / 60;
	sekunda = sekunda - (sekunda/60) * 60;
	stopinja = stopinja + sekunda / 60;
	
	/*
	printf("Vsota je : \n");
	printf("%d %d %d\n", stopinja, minuta, sekunda );
	*/
	
	double kot1, kot2, kot;
	kot1 = stopinja1 + minuta1/60.0 + sekunda1/60.0/60.0;
	kot2 = stopinja2 + minuta2/60.0 + sekunda2/60.0/60.0;
	double eps = 0.000001; // Stevilo za preprecitev napak pri zaokrozevanju
	kot = kot1 + kot2 + eps;
	
	stopinja = kot;
	minuta = kot * 60 - stopinja * 60;
	sekunda = kot * 60 * 60 - stopinja * 60 * 60 - minuta * 60;
	
	printf("Vsota %f + %f = %f\n", kot1, kot2, kot );
	printf("%d %d %d\n", stopinja, minuta, sekunda );

	return 0;
}
