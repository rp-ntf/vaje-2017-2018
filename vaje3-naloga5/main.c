#include <stdio.h>
/* Sestavi program, ki prebere naravno število n
 * in izpiše vsoto vseh njegovih števk.
 */

int main()
{
    int n;
    int enice;
    int vsota = 0;
    
	printf("Vpisi naravno stevilo:  "); scanf("%d",&n);
    
    while (n > 0) {
        enice = n % 10;
        vsota = vsota + enice;
        n = n / 10;
    }
    
    printf("Vsota stevk je %d.\n", vsota);
    
	return 0;
}
