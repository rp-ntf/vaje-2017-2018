#include <stdio.h>

/* Sestavi program, ki bere pozitivna cela števila >0,
 * dokler uporabnik ne vpiše števila nič.
 * Program na koncu izpiše maksimalno in 
 * minimalno vpisano vrednost
 */

int main()
{
	int vrednost, maksimum, minimum;
    
    printf("Vpisi stevilo:  "); scanf("%d",&vrednost);
    maksimum = vrednost;
    minimum = vrednost;
    
    while (vrednost != 0) {
        if (vrednost > maksimum) maksimum = vrednost;
        if (vrednost < minimum) minimum = vrednost;
        printf("Vpisi stevilo:  "); scanf("%d",&vrednost);
    }
    
    printf("Maksimalna vrednsot je %d.\n",maksimum);
    printf("Minimalna vrednsot je %d. \n", minimum);
    
	return 0;
}
