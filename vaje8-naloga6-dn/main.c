#include <stdio.h>

/* Domaca naloga : 
 * Sestavite program, ki prikazuje igralno polje za 4-v-vrsto, in omogoca igranje 
 * kot smo danes ze naredili za krizce in krozce.
 * Izpis igralnega polja naj bo narejen v funkciji.
 * 
 * Bonus naloga : 
 * program naj pove, ce je kje v igralnem polju zasledil 4 v eni vrsti. Stolpcev in diagonal ni potrebno testirati. 
 * Bonus 2 : enako se za stolpce in diagonale
 */

// 4-v-vrsto : polje je velikosti 7x5
void izpisiPolje4VVrsto( char igralnoPolje[5][7] )
{
    for( int i=0; i<5; ++i )
    {
        for( int j=0; j<7; ++j )
        {
            printf("%c ", igralnoPolje[i][j] );
        }
        printf("\n");
    }
}
void izpisiPolje4VVrstoFensi( char igralnoPolje[5][7] )
{
		printf("   " );
		for( int j=0; j<7; ++j )
			printf("| %d ", j );
		printf("|\n");
    for( int i=0; i<5; ++i )
    {
		printf("%d: ", i );
        for( int j=0; j<7; ++j )
        {
            printf("| %c ", igralnoPolje[i][j] );
        }
        printf("|\n");
    }
}

// Bonus naloga 1:
// preverimo, ali so v kateri vrstici 4 enaki znaki zaporedoma
// funkcija vrne zmagovalca, ce obstaja, ce ne vrne vrednost 0
char preveriVrstice( char igralnoPolje[5][7] )
{
	int n; // stevilo zaporednih enakih zetonov
	char zeton;
	for( int i=0; i<5; ++i )
	{
		zeton = igralnoPolje[i][0];
		n=1;
		for( int j=1; j<7; ++j )
		{
			if( igralnoPolje[i][j] == zeton )
			{
				n++;
				if( n >= 4 && zeton != ' ' )
				{
					return zeton; // nekdo je zmagal, ne rabimo naprej preverjat
				}
			}
			else
			{
				n=1;
				zeton = igralnoPolje[i][j];
			}
		}
	}
	return 0;
}
// Bonus naloga 2:
// isto za stolpce
// funkcija vrne zmagovalca, ce obstaja, ce ne vrne vrednost 0
char preveriStolpce( char igralnoPolje[5][7] )
{
	int n; // stevilo zaporednih enakih zetonov
	char zeton;
	for( int j=0; j<7; ++j )
	{
		zeton = igralnoPolje[0][j];
		n=1;
		for( int i=1; i<5; ++i )
		{
			if( igralnoPolje[i][j] == zeton )
			{
				n++;
				if( n >= 4 && zeton != ' ' )
				{
					return zeton; // nekdo je zmagal, ne rabimo naprej preverjat
				}
			}
			else
			{
				n=1;
				zeton = igralnoPolje[i][j];
			}
		}
	}
	return 0;
}
// isto za diagonale 
// funkcija vrne zmagovalca, ce obstaja, ce ne vrne vrednost 0
char preveriDiagonale( char igralnoPolje[5][7] )
{
	int n; // stevilo zaporednih enakih zetonov
	char zeton;
	int i,j;
	for( int diag = -7-5; 
		diag <= 7+5; 
		++diag )
	{
		for( i=0; i<5; ++i )
		{
			j=+1*i+diag;
			if( j<0 || j>6 )
			{
				n=0;
				zeton=' ';
				continue;
			}
			if( igralnoPolje[i][j] == zeton )
			{
				n++;
				if( n >= 4 && zeton != ' ' )
				{
					return zeton; // nekdo je zmagal, ne rabimo naprej preverjat
				}
			}
			else
			{
				n=1;
				zeton = igralnoPolje[i][j];
			}
		}
	}

	for( int diag = -7-5; 
		diag <= 7+5; 
		++diag )
	{
		for( i=0; i<5; ++i )
		{
			j=-1*i+diag;
			if( j<0 || j>6 )
			{
				n=0;
				zeton=' ';
				continue;
			}
			if( igralnoPolje[i][j] == zeton )
			{
				n++;
				if( n >= 4 && zeton != ' ' )
				{
					return zeton; // nekdo je zmagal, ne rabimo naprej preverjat
				}
			}
			else
			{
				n=1;
				zeton = igralnoPolje[i][j];
			}
		}
	}

	return 0;
}

int main()
{
    char igralnoPolje[5][7];
    for( int i=0; i<5; ++i )
        for( int j=0; j<7; ++j )
            igralnoPolje[i][j] = ' ';
	char naVrsti='O'; // kdo je na vrsti ? O ali X
	int stolpec;
	while(1)
	{
		// izpisiPolje4VVrsto( igralnoPolje );
		izpisiPolje4VVrstoFensi( igralnoPolje );
		
		// Bonus 
		char zmagal = 0;
		if( zmagal == 0 )
			zmagal = preveriVrstice( igralnoPolje );
		if( zmagal == 0 )
			zmagal = preveriStolpce( igralnoPolje );
		if( zmagal == 0 )
			zmagal = preveriDiagonale( igralnoPolje );
		if( zmagal != 0 )
		{
			printf("Zmagal je %c. Cestitke !\n", zmagal );
			break;
		}
		// konec bonusa
		
		printf("Na vrsti je %c. Vnesite stolpec (0-6)\n", naVrsti);
		scanf("%d", &stolpec );
		if( stolpec < 0 || stolpec > 6 )
		{
			printf("Stolpec je napacen. Mora biti med 0-6, -1 za zakljucek igre.\n");
			if( stolpec == -1 )
				return 0;
			continue;
		}
		int vrstica=0;
		// Najdemo zadnjo vrstico, ki je se prazna,
		// da zeton pade na tla:
		for( vrstica=0; 
			vrstica < 5 && igralnoPolje[vrstica][stolpec] == ' '; 
			++vrstica )
		{}
		if( vrstica == 0 )
		{
			printf("Poizkusili ste dodati zeton v poln stolpec %d. To ne gre. \n", stolpec );
			continue;
		}
		vrstica -= 1;
		igralnoPolje[vrstica][stolpec] = naVrsti;
		if( naVrsti == 'O' )
			naVrsti = 'X';
		else 
			naVrsti = 'O';
		
	}
	return 0;
}
